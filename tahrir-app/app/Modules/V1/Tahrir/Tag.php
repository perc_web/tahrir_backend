<?php namespace App\Modules\V1\Tahrir;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\V1\Tahrir\Traits\Translatable;

class Tag extends Model{

    use Translatable;
    use SoftDeletes;
    protected $table     = 'tags';
    protected $dates     = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden    = ['deleted_at'];
    protected $guarded   = ['id'];
    protected $fillable  = ['name'];
    public $searchable   = ['name->en', 'name->fr', 'name->sp'];
    public $translatable = ['name'];
    public $casts        = ['name' => 'json'];

    /**
     * Add time zone diff to created_at
     * 
     * @param  date $value
     * @return date
     */
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get(\CoreConfig::getConfig()['var_names']['timeZoneDiff']))->toDateTimeString();
    }

    /**
     * Add time zone diff to updated_at
     * 
     * @param  date $value
     * @return date
     */
    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get(\CoreConfig::getConfig()['var_names']['timeZoneDiff']))->toDateTimeString();
    }

    /**
     * Add time zone diff to deleted_at
     * 
     * @param  date $value
     * @return date
     */
    public function getDeletedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get(\CoreConfig::getConfig()['var_names']['timeZoneDiff']))->toDateTimeString();
    }

    /**
     * Return the tag trips.
     * 
     * @return colection
     */
    public function trips()
    {
        return $this->belongsToMany('\App\Modules\V1\Tahrir\Trip','trips_tags','tag_id','trip_id')->whereNull('trips_tags.deleted_at')->withTimestamps();
    }

    public static function boot()
    {
        parent::boot();
        parent::observe(\App::make('App\Modules\V1\Tahrir\ModelObservers\TagObserver'));
    }
}
