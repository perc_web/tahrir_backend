<?php namespace App\Modules\V1\Tahrir;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\V1\Tahrir\Traits\Translatable;

class Trip extends Model{

    use Translatable;
    use SoftDeletes;
    protected $table     = 'trips';
    protected $dates     = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden    = ['deleted_at', 'pivot', '_hotels', 'rooms'];
    protected $guarded   = ['id'];
    protected $fillable  = [
        'image',
        'title',
        'run',
        'duration',
        'overview',
        'include',
        'exclude',
        'program',
        'country_id',
        'city_id',
        'has_accommodation',
        'featured',
        'prices',
        'payment',
        'cancellation_policy',
        'children_policy',
        'tour_voucher',
        'tour_type',
        'start_point',
        'pickup_time',
        'lowest_price',
        'short_description',
        'order',
        'type',
        'childs_2_6',
        'childs_6_12',
        'pick_off',
        'drop_off',
        'hide',
        'number_of_itineraries'
    ];
    public $searchable   = [
        'title->en', 
        'title->fr', 
        'title->sp'
    ];
    public $translatable = [
        'title',
        'run',
        'duration',
        'overview',
        'include',
        'exclude',
        'program',
        'prices',
        'payment',
        'cancellation_policy',
        'children_policy',
        'tour_voucher',
        'tour_type',
        'start_point',
        'short_description',
        'pick_off',
        'drop_off'
    ];
    public $casts        = [
        'title'               => 'json',
        'run'                 => 'json',
        'duration'            => 'json',
        'overview'            => 'json',
        'include'             => 'json',
        'exclude'             => 'json',
        'program'             => 'json',
        'prices'              => 'json',
        'payment'             => 'json',
        'cancellation_policy' => 'json',
        'children_policy'     => 'json',
        'tour_voucher'        => 'json',
        'tour_type'           => 'json',
        'start_point'         => 'json',
        'short_description'   => 'json',
        'pick_off'            => 'json',
        'drop_off'            => 'json',
        'lowest_price'        => 'integer'
    ];

    /**
     * Add time zone diff to created_at
     * 
     * @param  date $value
     * @return date
     */
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get(\CoreConfig::getConfig()['var_names']['timeZoneDiff']))->toDateTimeString();
    }

    /**
     * Add time zone diff to updated_at
     * 
     * @param  date $value
     * @return date
     */
    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get(\CoreConfig::getConfig()['var_names']['timeZoneDiff']))->toDateTimeString();
    }

    /**
     * Add time zone diff to deleted_at
     * 
     * @param  date $value
     * @return date
     */
    public function getDeletedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get(\CoreConfig::getConfig()['var_names']['timeZoneDiff']))->toDateTimeString();
    }

    /**
     * Get the pickup_time.
     * @return time
     */
    public function getPickupTimeAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['pickup_time'])->format('H:i');
    }

    /**
     * Get the image url.
     * @return string
     */
    public function getImageAttribute($value)
    {
        return $value ? url($value) : '';
    }

    /**
     * Return the trip categories.
     * 
     * @return colection
     */
    public function categories()
    {
        return $this->belongsToMany('\App\Modules\V1\Tahrir\Category','trips_categories','trip_id','category_id')->whereNull('trips_categories.deleted_at')->withTimestamps();
    }

    /**
     * Return the trip tags.
     * 
     * @return colection
     */
    public function tags()
    {
        return $this->belongsToMany('\App\Modules\V1\Tahrir\Tag','trips_tags','trip_id','tag_id')->whereNull('trips_tags.deleted_at')->withTimestamps();
    }

    /**
     * Return the trip images.
     * 
     * @return colection
     */
    public function tripImages()
    {
        return $this->hasMany('App\Modules\V1\Tahrir\TripImage', 'trip_id');
    }

    /**
     * Return the trip reservations.
     * 
     * @return colection
     */
    public function tripReservations()
    {
        return $this->hasMany('App\Modules\V1\Tahrir\TripReservation', 'trip_id');
    }

    /**
     * Return the trip rooms.
     * 
     * @return colection
     */
    public function rooms()
    {
        return $this->belongsToMany('\App\Modules\V1\Tahrir\Room','trips_rooms','trip_id','room_id')->withPivot('hotel_id', 'price', 'count', 'reserved', 'avaialble')->whereNull('trips_rooms.deleted_at')->withTimestamps();
    }

    /**
     * Return the trip hotels.
     * 
     * @return colection
     */
    public function _hotels()
    {
        return $this->belongsToMany('\App\Modules\V1\Tahrir\Hotel','trips_rooms','trip_id','hotel_id')->distinct()->whereNull('trips_rooms.deleted_at')->withTimestamps();
    }

    /**
     * Return the trip related.
     * 
     * @return colection
     */
    public function related()
    {
        return $this->belongsToMany('\App\Modules\V1\Tahrir\Trip','trips_related','trip_id','related_id')->whereNull('trips_related.deleted_at')->withTimestamps();
    }

    /**
     * Return the trip country.
     * 
     * @return colection
     */
    public function country()
    {
        return $this->belongsTo('App\Modules\V1\Tahrir\Country');
    }

    /**
     * Return the trip city.
     * 
     * @return colection
     */
    public function city()
    {
        return $this->belongsTo('App\Modules\V1\Tahrir\City');
    }

    public static function boot()
    {
        parent::boot();
        parent::observe(\App::make('App\Modules\V1\Tahrir\ModelObservers\TripObserver'));
    }
}
