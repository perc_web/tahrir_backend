<?php
namespace App\Modules\V1\Tahrir\Http\Controllers;

use Illuminate\Foundation\Http\FormRequest;
use App\Modules\V1\Core\Http\Controllers\BaseApiController;
use Illuminate\Http\Request;


class HomeTabsController extends BaseApiController
{
    /**
     * The name of the model that is used by the base api controller 
     * to preform actions like (add, edit ... etc).
     * @var string
     */
    protected $model           = 'homeTabs';
    
    /**
     * List of all route actions that the base api controller
     * will skip login check for them.
     * @var array
     */
    protected $skipLoginCheck  = ['find', 'list', 'search', 'findby', 'first', 'paginate', 'paginateby'];

    /**
     * The validations rules used by the base api controller
     * to check before add.
     * @var array
     */
    protected $validationRules = [
        'description'    => 'required|array',
        'description.en' => 'required|string',
        'description.fr' => 'required|string',
        'description.sp' => 'required|string',
    ];

}
