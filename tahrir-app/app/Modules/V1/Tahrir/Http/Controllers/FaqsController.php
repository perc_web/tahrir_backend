<?php
namespace App\Modules\V1\Tahrir\Http\Controllers;

use Illuminate\Foundation\Http\FormRequest;
use App\Modules\V1\Core\Http\Controllers\BaseApiController;
use Illuminate\Http\Request;


class FaqsController extends BaseApiController
{
    /**
     * The name of the model that is used by the base api controller 
     * to preform actions like (add, edit ... etc).
     * @var string
     */
    protected $model           = 'faqs';
    
    /**
     * List of all route actions that the base api controller
     * will skip login check for them.
     * @var array
     */
    protected $skipLoginCheck  = ['find', 'list', 'search', 'findby', 'first', 'paginate', 'paginateby'];

    /**
     * The validations rules used by the base api controller
     * to check before add.
     * @var array
     */
    protected $validationRules = [
        'question'    => 'required|array',
        'question.en' => 'required|string|max:255',
        'question.fr' => 'required|string|max:255',
        'question.sp' => 'required|string|max:255',
        'answer'      => 'required|array',
        'answer.en'   => 'required|string',
        'answer.fr'   => 'required|string',
        'answer.sp'   => 'required|string',
    ];

}
