<?php
namespace App\Modules\V1\Tahrir\Http\Controllers\Payfort;

use Illuminate\Foundation\Http\FormRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;


class PayfortController extends Controller
{   
    /**
     * Redirect to payfort merchant page.
     * 
     * @param  integer $tripReservationId
     * @param  string  $language
     * @param  string  $tokenName
     */
    public function redirect($tripReservationId, $language, $tokenName = false)
    {
        return \Payfort::redirect($tripReservationId, $language, $tokenName);
    }

    /**
     * Redirect to payfort merchant page.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function response(Request $request)
    {
        return \Payfort::response($request->all());
    }
}
