<?php
namespace App\Modules\V1\Tahrir\Http\Controllers;

use Illuminate\Foundation\Http\FormRequest;
use App\Modules\V1\Core\Http\Controllers\BaseApiController;
use Illuminate\Http\Request;


class ContactController extends BaseApiController
{
    protected $model = '';

    /**
     * List of all route actions that the base api controller
     * will skip login check for them.
     * @var array
     */
    protected $skipLoginCheck  = ['send', 'enquire'];

    /**
     * Send contact us email.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function send(Request $request) 
    {
        $this->validate($request, [
            'email'            => 'required|email',
            'name'             => 'required|string|max:100', 
            'message'          => 'required|string',
            'phone'            => 'nullable',
            'country'          => 'nullable',
            'city'             => 'nullable',
            'from'             => 'nullable',
            'to'               => 'nullable',
            'adults_numbers'   => 'nullable',
            'childs6_numbers'  => 'nullable',
            'childs12_numbers' => 'nullable',
            'description'      => 'nullable',
        ]);

        $sender = $request->all();
        
        \Mail::send('tahrir::contact_us', compact('sender'), function ($m) use ($sender) {
            foreach (\Core::settings()->first(['settings.key' => 'contact_emails'])->value as $email) 
            {
                $m->to($email)->from($sender['email'], $sender['name'])->subject('Contact Us Form');
            }
        });

       return \Response::json('success', 200);
    }

    /**
     * Send enquire email.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function enquire(Request $request) 
    {
        $this->validate($request, [
            'price'               => 'required|numeric',
            'email'               => 'required|email',
            'name'                => 'required|string|max:100', 
            'description'         => 'nullable|string',
            'phone'               => 'required|string',
            'nationality'         => 'required|string',
            'tour_language'       => 'nullable|string',
            'from'                => 'nullable|date',
            'reservation_details' => 'nullable|array',
            'trip_id'             => 'required|exists:trips,id'
        ]);

        $data         = $request->all();
        $data['trip'] = \Core::tripReservations()->find($data['trip_id']);
        \Mail::send('tahrir::enquire', compact('data'), function ($m) use ($data) {
            foreach (\Core::settings()->first(['settings.key' => 'enquiry_emails'])->value as $email) 
            {
                $m->to($email)->from($data['email'], $data['name'])->subject('Enquire Form');
            }
        });

       return \Response::json('success', 200);
    }
}
