<?php
namespace App\Modules\V1\Tahrir\Http\Controllers;

use Illuminate\Foundation\Http\FormRequest;
use App\Modules\V1\Core\Http\Controllers\BaseApiController;
use Illuminate\Http\Request;


class HotelsController extends BaseApiController
{
    /**
     * The name of the model that is used by the base api controller 
     * to preform actions like (add, edit ... etc).
     * @var string
     */
    protected $model           = 'hotels';
    
    /**
     * List of all route actions that the base api controller
     * will skip login check for them.
     * @var array
     */
    protected $skipLoginCheck  = ['find', 'list', 'search', 'findby', 'first', 'paginate', 'paginateby'];

    /**
     * The validations rules used by the base api controller
     * to check before add.
     * @var array
     */
    protected $validationRules = [
        'image'        => 'required|string|max:150',
        'name'         => 'required|array',
        'name.en'      => 'required|string|max:100',
        'name.fr'      => 'required|string|max:100',
        'name.sp'      => 'required|string|max:100',
        'rate_word'    => 'required|array',
        'rate_word.en' => 'required|string|max:100',
        'rate_word.fr' => 'required|string|max:100',
        'rate_word.sp' => 'required|string|max:100',
        'stars'        => 'required|integer',
        'rooms'        => 'required|array',
        'rooms.*.id'   => 'required|exists:rooms,id',
    ];

}
