<?php
namespace App\Modules\V1\Tahrir\Http\Controllers;

use Illuminate\Foundation\Http\FormRequest;
use App\Modules\V1\Core\Http\Controllers\BaseApiController;
use Illuminate\Http\Request;


class MediaController extends BaseApiController
{
    protected $model = '';
    public function __construct()
    {
       \JWTAuth::parseToken()->authenticate();
    }

    /**
     * Upload the given image.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function uploadImage(Request $request) 
    {
       $this->validate($request, ['image' => 'required|image', 'dir' => 'required|string|max:10']);

       $response        = [];
       $image           = $request->file('image');
       $imageName       =  str_slug('image' . uniqid() . time() . '_' . $image->getClientOriginalName());
       $destinationPath = 'media' . DIRECTORY_SEPARATOR . $request->get('dir') . DIRECTORY_SEPARATOR;

       ! file_exists($destinationPath) ? \File::makeDirectory($destinationPath) : false;
       $image->move($destinationPath, $imageName);

       $response['path'] = url($destinationPath . $imageName);

       return \Response::json($response, 200);
    }

    /**
     * Upload base64 image .
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function uploadImageBas64(Request $request) 
    {
        $this->validate($request, ['image' => 'required', 'dir' => 'required|string|max:10']);

        $response        = [];
        $image           = $request->get('image');
        $imageName       = 'image' . uniqid() . time() . '.jpg';
        $destinationPath = 'media' . DIRECTORY_SEPARATOR . $request->get('dir') . DIRECTORY_SEPARATOR;
        
        ! file_exists($destinationPath) ? \File::makeDirectory($destinationPath) : false;
        
        $base             = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $image));
        $image            = \Image::make($base)->save($destinationPath . $imageName);
        $response['path'] = url($destinationPath . $imageName);

        return \Response::json($response, 200);
    }

    /**
     * Delete the given image.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function deleteImage(Request $request) 
    {
        $this->validate($request, ['path' => 'required|string', 'dir' => 'required|string|max:10']);
        
        $response = [];
        $arr      = explode('/', $request->get('path'));
        $path     = 'media' . DIRECTORY_SEPARATOR . $request->get('dir') . DIRECTORY_SEPARATOR . end($arr);
        if (\File::exists($path)) 
        {
            \File::delete($path);
        }

        return \Response::json($response, 200);
    }
}
