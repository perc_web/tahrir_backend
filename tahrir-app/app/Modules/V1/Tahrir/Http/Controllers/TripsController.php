<?php
namespace App\Modules\V1\Tahrir\Http\Controllers;

use Illuminate\Foundation\Http\FormRequest;
use App\Modules\V1\Core\Http\Controllers\BaseApiController;
use Illuminate\Http\Request;


class TripsController extends BaseApiController
{
    /**
     * The name of the model that is used by the base api controller 
     * to preform actions like (add, edit ... etc).
     * @var string
     */
    protected $model           = 'trips';
    
    /**
     * List of all route actions that the base api controller
     * will skip login check for them.
     * @var array
     */
    protected $skipLoginCheck  = ['find', 'list', 'search', 'findby', 'first', 'paginate', 'paginateby', 'featured', 'getItineraries'];

    /**
     * Transform the respones using the given transformer class.
     * @var string
     */
    protected $transformer  = 'App\Modules\V1\Tahrir\Transfomers\TripTransformer';

    /**
     * The validations rules used by the base api controller
     * to check before add.
     * @var array
     */
    protected $validationRules = [
        'image'                  => 'required|string|max:150',
        'title'                  => 'required|array',
        'title.en'               => 'required|string|max:100',
        'title.fr'               => 'required|string|max:100',
        'title.sp'               => 'required|string|max:100',
        'run'                    => 'required|array',
        'run.en'                 => 'required|string|max:100',
        'run.fr'                 => 'required|string|max:100',
        'run.sp'                 => 'required|string|max:100',
        'duration'               => 'required|array',
        'duration.en'            => 'required|string|max:100',
        'duration.fr'            => 'required|string|max:100',
        'duration.sp'            => 'required|string|max:100',
        'overview'               => 'required|array',
        'overview.en'            => 'required|string',
        'overview.fr'            => 'required|string',
        'overview.sp'            => 'required|string',
        'include'                => 'required|array',
        'include.en'             => 'required|string',
        'include.fr'             => 'required|string',
        'include.sp'             => 'required|string',
        'exclude'                => 'required|array',
        'exclude.en'             => 'required|string',
        'exclude.fr'             => 'required|string',
        'exclude.sp'             => 'required|string',
        'cancellation_policy'    => 'required|array',
        'cancellation_policy.en' => 'required|string',
        'cancellation_policy.fr' => 'required|string',
        'cancellation_policy.sp' => 'required|string',
        'payment'                => 'required|array',
        'payment.en'             => 'required|string',
        'payment.fr'             => 'required|string',
        'payment.sp'             => 'required|string',
        'children_policy'        => 'required|array',
        'children_policy.en'     => 'required|string',
        'children_policy.fr'     => 'required|string',
        'children_policy.sp'     => 'required|string',
        'tour_voucher'           => 'required|array',
        'tour_voucher.en'        => 'required|string',
        'tour_voucher.fr'        => 'required|string',
        'tour_voucher.sp'        => 'required|string',
        'tour_type'              => 'required|array',
        'tour_type.en'           => 'required|string',
        'tour_type.fr'           => 'required|string',
        'tour_type.sp'           => 'required|string',
        'start_point'            => 'required|array',
        'start_point.en'         => 'required|string|max:100',
        'start_point.fr'         => 'required|string|max:100',
        'start_point.sp'         => 'required|string|max:100',
        'country_id'             => 'required|exists:countries,id',
        'city_id'                => 'required|exists:cities,id',
        'has_accommodation'      => 'boolean',
        'featured'               => 'boolean',
        'pickup_time'            => 'required|date_format:H:i',
        'short_description'      => 'required|array',
        'short_description.en'   => 'required|string|max:400',
        'short_description.fr'   => 'required|string|max:400',
        'short_description.sp'   => 'required|string|max:400',
        'lowest_price'           => 'required|numeric',
        'type'                   => 'required|in:trip,accommodation,transfer',
        'prices'                 => 'required|array',
        'prices.*.from'          => 'required|integer',
        'prices.*.to'            => 'required|integer',
        'prices.*.price'         => 'required|numeric',
        'childs_2_6'             => 'nullable|numeric',
        'childs_6_12'            => 'nullable|numeric',
        'pick_off'               => 'array',
        'pick_off.en'            => 'required|string',
        'pick_off.fr'            => 'required|string',
        'pick_off.sp'            => 'required|string',
        'drop_off'               => 'array',
        'drop_off.en'            => 'required_with:drop_off|string',
        'drop_off.fr'            => 'required_with:drop_off|string',
        'drop_off.sp'            => 'required_with:drop_off|string',
        'hide'                   => 'nullable|boolean',
        'number_of_itineraries'  => 'nullable|integer'
        /*'program'                  => 'required|array',
        'program.*.en'             => 'required|array',
        'program.*.en.title'       => 'required|string|max:100',
        'program.*.en.description' => 'required|string',
        'program.*.en.image'       => 'required|string',
        'program.*.fr'             => 'required|array',
        'program.*.fr.title'       => 'required|string|max:100',
        'program.*.fr.description' => 'required|string',
        'program.*.fr.image'       => 'required|string',
        'program.*.sp'             => 'required|array',
        'program.*.sp.title'       => 'required|string|max:100',
        'program.*.sp.description' => 'required|string',
        'program.*.sp.image'       => 'required|string',
        'hotels'                   => 'required_if:has_accommodation,1|array',
        'hotels.*.id'              => 'required|exists:hotels,id',
        'hotels.*.rooms'           => 'required,1|array',
        'hotels.*.rooms.*.id'      => 'required|exists:rooms,id',
        'hotels.*.rooms.*.price'   => 'required|numeric',
        'hotels.*.rooms.*.count'   => 'required|integer',
        'categories'               => 'required|array',
        'categories.*.id'          => 'required|exists:categories,id',
        'related'                  => 'array',
        'related.*.id'             => 'required|exists:trips,id',*/
    ];

    /**
     * Return featured trips in random order.
     * 
     * @param  integer $count Number of rows default 3.
     * @return \Illuminate\Http\Response
     */
    public function featured($count = 3) 
    {
        if ($this->repo) 
        {
            if ($this->transformer) 
            {
                return \Response::json($this->transformer->transform($this->repo->featured($count, $this->relations)), 200);
            }
            
            return \Response::json($this->repo->featured($count, $this->relations), 200);
        }
    }

    /**
     * Get all avaialbel itineraries;
     * 
     * @return \Illuminate\Http\Response
     */
    public function getItineraries()
    {
        return $this->repo->getItineraries();
    }
}