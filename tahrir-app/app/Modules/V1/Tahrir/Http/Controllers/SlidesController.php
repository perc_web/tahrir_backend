<?php
namespace App\Modules\V1\Tahrir\Http\Controllers;

use Illuminate\Foundation\Http\FormRequest;
use App\Modules\V1\Core\Http\Controllers\BaseApiController;
use Illuminate\Http\Request;


class SlidesController extends BaseApiController
{
    /**
     * The name of the model that is used by the base api controller 
     * to preform actions like (add, edit ... etc).
     * @var string
     */
    protected $model           = 'slides';
    
    /**
     * List of all route actions that the base api controller
     * will skip login check for them.
     * @var array
     */
    protected $skipLoginCheck  = ['find', 'list', 'search', 'findby', 'first', 'paginate', 'paginateby'];

    /**
     * The validations rules used by the base api controller
     * to check before add.
     * @var array
     */
    protected $validationRules = [
        'image'                => 'required|string|max:150',
        'trip_notes'           => 'required|array',
        'trip_notes.en'        => 'required|string|max:255',
        'trip_notes.fr'        => 'required|string|max:255',
        'trip_notes.sp'        => 'required|string|max:255',
        'short_description'    => 'required|array',
        'short_description.en' => 'required|string|max:255',
        'short_description.fr' => 'required|string|max:255',
        'short_description.sp' => 'required|string|max:255',
        'hide'                 => 'boolean',
        'position'             => 'boolean',
    ];

}
