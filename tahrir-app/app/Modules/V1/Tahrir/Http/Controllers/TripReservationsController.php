<?php
namespace App\Modules\V1\Tahrir\Http\Controllers;

use Illuminate\Foundation\Http\FormRequest;
use App\Modules\V1\Core\Http\Controllers\BaseApiController;
use Illuminate\Http\Request;


class TripReservationsController extends BaseApiController
{
    /**
     * The name of the model that is used by the base api controller 
     * to preform actions like (add, edit ... etc).
     * @var string
     */
    protected $model = 'tripReservations';
    
    /**
     * List of all route actions that the base api controller
     * will skip login check for them.
     * @var array
     */
    protected $skipLoginCheck  = ['reserve'];

    /**
     * Reserve the given trip.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function reserve(Request $request) 
    {
    	$this->validate($request, [
            'price'                 => 'required|numeric',
            'email'                 => 'required|email',
            'name'                  => 'required|string|max:100', 
            'description'           => 'nullable|string',
            'phone'                 => 'required|string',
            'nationality'           => 'required|string',
            'tour_language'         => 'nullable|string',
            'from'                  => 'nullable|date',
            'accommodation'         => 'nullable|string',
            'number_of_adults'      => 'nullable|integer',
            'number_of_childs_2_6'  => 'nullable|integer',
            'number_of_childs_6_12' => 'nullable|integer',
            'childs'                => 'nullable|integer',
            'should_pay'            => 'boolean',
            'baby_chair'            => 'nullable|boolean',
            'trip_id'               => 'required|exists:trips,id'
    	]);

        return \Response::json($this->repo->save($request->only('price', 'name', 'email', 'phone', 'nationality', 'description', 'tour_language', 'from', 'reservation_details', 'accommodation', 'number_of_adults', 'number_of_childs_2_6', 'number_of_childs_6_12', 'baby_chair', 'childs', 'should_pay', 'trip_id'), false), 200);
    }

    /**
     * Mark the reserevation as seen.
     * 
     * @param  integer $id
     * @return \Illuminate\Http\Response
     */
    public function seen($id) 
    {
        return \Response::json($this->repo->seen($id), 200);
    }
}