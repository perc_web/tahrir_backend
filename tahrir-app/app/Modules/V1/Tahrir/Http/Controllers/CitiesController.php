<?php
namespace App\Modules\V1\Tahrir\Http\Controllers;

use Illuminate\Foundation\Http\FormRequest;
use App\Modules\V1\Core\Http\Controllers\BaseApiController;
use Illuminate\Http\Request;


class CitiesController extends BaseApiController
{
    /**
     * The name of the model that is used by the base api controller 
     * to preform actions like (add, edit ... etc).
     * @var string
     */
    protected $model           = 'cities';
    
    /**
     * List of all route actions that the base api controller
     * will skip login check for them.
     * @var array
     */
    protected $skipLoginCheck  = ['find', 'list', 'search', 'findby', 'first', 'paginate', 'paginateby', 'homePage'];

    /**
     * The validations rules used by the base api controller
     * to check before add.
     * @var array
     */
    protected $validationRules = [
        'image'             => 'required|string|max:150',
        'name'              => 'required|array',
        'name.en'           => 'required|string|max:100',
        'name.fr'           => 'required|string|max:100',
        'name.sp'           => 'required|string|max:100',
        'country_id'        => 'required|exists:countries,id',
        'show_in_home_page' => 'boolean'
    ];

    /**
     * Return home page cities in random order.
     * 
     * @param  integer $count Number of rows default 3.
     * @return \Illuminate\Http\Response
     */
    public function homePage($count = 3) 
    {
        if ($this->repo) 
        {
            return \Response::json($this->repo->homePage($count, $this->relations), 200);
        }
    }
}
