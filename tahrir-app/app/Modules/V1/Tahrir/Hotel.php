<?php namespace App\Modules\V1\Tahrir;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\V1\Tahrir\Traits\Translatable;

class Hotel extends Model{

    use Translatable;
    use SoftDeletes;
    protected $table     = 'hotels';
    protected $dates     = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden    = ['deleted_at', 'pivot'];
    protected $guarded   = ['id'];
    protected $fillable  = ['image', 'name', 'rate_word', 'stars'];
    public $searchable   = ['name->en', 'name->fr', 'name->sp', 'rate_word->en', 'rate_word->fr', 'rate_word->sp'];
    public $translatable = ['name', 'rate_word'];
    public $casts        = ['name' => 'json', 'rate_word' => 'json'];

    /**
     * Add time zone diff to created_at
     * 
     * @param  date $value
     * @return date
     */
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get(\CoreConfig::getConfig()['var_names']['timeZoneDiff']))->toDateTimeString();
    }

    /**
     * Add time zone diff to updated_at
     * 
     * @param  date $value
     * @return date
     */
    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get(\CoreConfig::getConfig()['var_names']['timeZoneDiff']))->toDateTimeString();
    }

    /**
     * Add time zone diff to deleted_at
     * 
     * @param  date $value
     * @return date
     */
    public function getDeletedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get(\CoreConfig::getConfig()['var_names']['timeZoneDiff']))->toDateTimeString();
    }

    /**
     * Get the image url.
     * @return string
     */
    public function getImageAttribute($value)
    {
        return $value ? url($value) : '';
    }

    /**
     * Return the hotel rooms.
     * 
     * @return colection
     */
    public function rooms()
    {
        return $this->belongsToMany('\App\Modules\V1\Tahrir\Room','hotels_rooms','hotel_id','room_id')->whereNull('hotels_rooms.deleted_at')->withTimestamps();
    }

    public static function boot()
    {
        parent::boot();
        parent::observe(\App::make('App\Modules\V1\Tahrir\ModelObservers\HotelObserver'));
    }
}
