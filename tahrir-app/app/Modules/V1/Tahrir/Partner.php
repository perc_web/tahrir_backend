<?php namespace App\Modules\V1\Tahrir;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\V1\Tahrir\Traits\Translatable;

class Partner extends Model{

    use SoftDeletes;
    protected $table     = 'partners';
    protected $dates     = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden    = ['deleted_at'];
    protected $guarded   = ['id'];
    protected $fillable  = ['image', 'hide', 'position'];
    public $searchable   = [];

    /**
     * Add time zone diff to created_at
     * 
     * @param  date $value
     * @return date
     */
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get(\CoreConfig::getConfig()['var_names']['timeZoneDiff']))->toDateTimeString();
    }

    /**
     * Add time zone diff to updated_at
     * 
     * @param  date $value
     * @return date
     */
    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get(\CoreConfig::getConfig()['var_names']['timeZoneDiff']))->toDateTimeString();
    }

    /**
     * Add time zone diff to deleted_at
     * 
     * @param  date $value
     * @return date
     */
    public function getDeletedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get(\CoreConfig::getConfig()['var_names']['timeZoneDiff']))->toDateTimeString();
    }

    /**
     * Get the image url.
     * @return string
     */
    public function getImageAttribute($value)
    {
        return $value ? url($value) : '';
    }

    public static function boot()
    {
        parent::boot();
        parent::observe(\App::make('App\Modules\V1\Tahrir\ModelObservers\PartnerObserver'));
    }
}
