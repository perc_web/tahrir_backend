<?php
namespace App\Modules\V1\Tahrir\Transfomers;

class TripTransformer
{
    public function transform($data)
    {
        if ($data) 
        {
            if($data instanceof \Illuminate\Database\Eloquent\Collection || $data instanceof \Illuminate\Pagination\LengthAwarePaginator) 
            {
                $data->map(function ($object){
                    return $this->transformObject($object);
                });
            } 
            else 
            {
                $data = $this->transformObject($data);
            }
        }

        return $data;
    }

    protected function transformObject($object)
    {
        $hotels = [];
        $object->_hotels->each(function($hotel) use ($object, &$hotels){
            $hotel = $hotel->toArray();
            $object->rooms->whereIn('pivot.hotel_id', [$hotel['id']])->each(function($room) use (&$hotel){
                $hotel['rooms'][] = [
                    'id'    => $room->id,
                    'type'  => $room->type,
                    'price' => (double)$room->pivot->price, 
                    'count' => $room->pivot->count
                ];
            });
            $hotels[] = $hotel;
        });
        $object->hotels = $hotels;

        return $object;
    }
}