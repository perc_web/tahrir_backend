<?php namespace App\Modules\V1\Tahrir\ModelObservers;

/**
 * Handling of model events,
 */
class TripReservationObserver {

    public function saving($model)
    {
        //
    }

    public function saved($model)
    {
        //
    }

    public function creating($model)
    {
        //
    }

    public function created($model)
    {
        if ( ! $model->should_pay) 
        {
            $model->state = 'complete';
            $model->save();
        }
    }

    public function updating($model)
    {
        //
    }

    public function updated($model)
    {
        if ($model->isDirty('state') && $model->state == 'complete') 
        {
            $tripReservation = $model;
            \Mail::send('tahrir::enquire', compact('tripReservation'), function ($m) use ($model) {
                foreach (\Core::settings()->first(['settings.key' => 'contact_emails'])->value as $email) 
                {
                    $m->to($email)->from($model->email, $model->name)->subject('Contact Us Form');
                }
            });
        }
    }

    public function deleting($model)
    {
        //
    }

    public function deleted($model)
    {
        //
    }

    public function restoring($model)
    {
        //
    }

    public function restored($model)
    {
        //
    }
}