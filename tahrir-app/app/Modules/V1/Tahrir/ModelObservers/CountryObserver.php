<?php namespace App\Modules\V1\Tahrir\ModelObservers;

/**
 * Handling of model events,
 */
class CountryObserver {

    public function saving($model)
    {
        //
    }

    public function saved($model)
    {
        //
    }

    public function creating($model)
    {
        //
    }

    public function created($model)
    {
        //
    }

    public function updating($model)
    {
        //
    }

    public function updated($model)
    {
        //
    }

    public function deleting($model)
    {
        //$model->cities()->delete();
    }

    public function deleted($model)
    {
        //
    }

    public function restoring($model)
    {
        //$model->cities()->restore();
    }

    public function restored($model)
    {
        //
    }
}