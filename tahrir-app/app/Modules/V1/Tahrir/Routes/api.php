<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your module. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'v1/tahrir'], function() {

	Route::group(['prefix' => 'countries'], function() {
		Route::get('list/{sortBy?}/{desc?}', 'CountriesController@index');
		Route::get('find/{id}', 'CountriesController@find');
		Route::get('search/{query?}/{perPage?}/{sortBy?}/{desc?}', 'CountriesController@search');
		Route::get('paginate/{perPage?}/{sortBy?}/{desc?}', 'CountriesController@paginate');
		Route::get('delete/{id}', 'CountriesController@delete');
		Route::get('restore/{id}', 'CountriesController@restore');
		Route::post('first', 'CountriesController@first');
		Route::post('findby/{sortBy?}/{desc?}', 'CountriesController@findby');
		Route::post('paginateby/{perPage?}/{sortBy?}/{desc?}', 'CountriesController@paginateby');
		Route::post('save', 'CountriesController@save');
		Route::post('deleted/{perPage?}/{sortBy?}/{desc?}', 'CountriesController@deleted');
	});

	Route::group(['prefix' => 'cities'], function() {
		Route::get('list/{sortBy?}/{desc?}', 'CitiesController@index');
		Route::get('find/{id}', 'CitiesController@find');
		Route::get('search/{query?}/{perPage?}/{sortBy?}/{desc?}', 'CitiesController@search');
		Route::get('paginate/{perPage?}/{sortBy?}/{desc?}', 'CitiesController@paginate');
		Route::get('delete/{id}', 'CitiesController@delete');
		Route::get('restore/{id}', 'CitiesController@restore');
		Route::get('home/{count?}', 'CitiesController@homePage');
		Route::post('first', 'CitiesController@first');
		Route::post('findby/{sortBy?}/{desc?}', 'CitiesController@findby');
		Route::post('paginateby/{perPage?}/{sortBy?}/{desc?}', 'CitiesController@paginateby');
		Route::post('save', 'CitiesController@save');
		Route::post('deleted/{perPage?}/{sortBy?}/{desc?}', 'CitiesController@deleted');
	});

	Route::group(['prefix' => 'categories'], function() {
		Route::get('list/{sortBy?}/{desc?}', 'CategoriesController@index');
		Route::get('find/{id}', 'CategoriesController@find');
		Route::get('search/{query?}/{perPage?}/{sortBy?}/{desc?}', 'CategoriesController@search');
		Route::get('paginate/{perPage?}/{sortBy?}/{desc?}', 'CategoriesController@paginate');
		Route::get('delete/{id}', 'CategoriesController@delete');
		Route::get('restore/{id}', 'CategoriesController@restore');
		Route::post('first', 'CategoriesController@first');
		Route::post('findby/{sortBy?}/{desc?}', 'CategoriesController@findby');
		Route::post('paginateby/{perPage?}/{sortBy?}/{desc?}', 'CategoriesController@paginateby');
		Route::post('save', 'CategoriesController@save');
		Route::post('deleted/{perPage?}/{sortBy?}/{desc?}', 'CategoriesController@deleted');
	});

	Route::group(['prefix' => 'home/tabs'], function() {
		Route::get('list/{sortBy?}/{desc?}', 'HomeTabsController@index');
		Route::get('find/{id}', 'HomeTabsController@find');
		Route::get('search/{query?}/{perPage?}/{sortBy?}/{desc?}', 'HomeTabsController@search');
		Route::get('paginate/{perPage?}/{sortBy?}/{desc?}', 'HomeTabsController@paginate');
		Route::get('delete/{id}', 'HomeTabsController@delete');
		Route::get('restore/{id}', 'HomeTabsController@restore');
		Route::post('first', 'HomeTabsController@first');
		Route::post('findby/{sortBy?}/{desc?}', 'HomeTabsController@findby');
		Route::post('paginateby/{perPage?}/{sortBy?}/{desc?}', 'HomeTabsController@paginateby');
		Route::post('save', 'HomeTabsController@save');
		Route::post('deleted/{perPage?}/{sortBy?}/{desc?}', 'HomeTabsController@deleted');
	});

	Route::group(['prefix' => 'hotels'], function() {
		Route::get('list/{sortBy?}/{desc?}', 'HotelsController@index');
		Route::get('find/{id}', 'HotelsController@find');
		Route::get('search/{query?}/{perPage?}/{sortBy?}/{desc?}', 'HotelsController@search');
		Route::get('paginate/{perPage?}/{sortBy?}/{desc?}', 'HotelsController@paginate');
		Route::get('delete/{id}', 'HotelsController@delete');
		Route::get('restore/{id}', 'HotelsController@restore');
		Route::post('first', 'HotelsController@first');
		Route::post('findby/{sortBy?}/{desc?}', 'HotelsController@findby');
		Route::post('paginateby/{perPage?}/{sortBy?}/{desc?}', 'HotelsController@paginateby');
		Route::post('save', 'HotelsController@save');
		Route::post('deleted/{perPage?}/{sortBy?}/{desc?}', 'HotelsController@deleted');
	});

	Route::group(['prefix' => 'partners'], function() {
		Route::get('list/{sortBy?}/{desc?}', 'PartnersController@index');
		Route::get('find/{id}', 'PartnersController@find');
		Route::get('search/{query?}/{perPage?}/{sortBy?}/{desc?}', 'PartnersController@search');
		Route::get('paginate/{perPage?}/{sortBy?}/{desc?}', 'PartnersController@paginate');
		Route::get('delete/{id}', 'PartnersController@delete');
		Route::get('restore/{id}', 'PartnersController@restore');
		Route::post('first', 'PartnersController@first');
		Route::post('findby/{sortBy?}/{desc?}', 'PartnersController@findby');
		Route::post('paginateby/{perPage?}/{sortBy?}/{desc?}', 'PartnersController@paginateby');
		Route::post('save', 'PartnersController@save');
		Route::post('deleted/{perPage?}/{sortBy?}/{desc?}', 'PartnersController@deleted');
	});

	Route::group(['prefix' => 'rooms'], function() {
		Route::get('list/{sortBy?}/{desc?}', 'RoomsController@index');
		Route::get('find/{id}', 'RoomsController@find');
		Route::get('search/{query?}/{perPage?}/{sortBy?}/{desc?}', 'RoomsController@search');
		Route::get('paginate/{perPage?}/{sortBy?}/{desc?}', 'RoomsController@paginate');
		Route::get('delete/{id}', 'RoomsController@delete');
		Route::get('restore/{id}', 'RoomsController@restore');
		Route::post('first', 'RoomsController@first');
		Route::post('findby/{sortBy?}/{desc?}', 'RoomsController@findby');
		Route::post('paginateby/{perPage?}/{sortBy?}/{desc?}', 'RoomsController@paginateby');
		Route::post('save', 'RoomsController@save');
		Route::post('deleted/{perPage?}/{sortBy?}/{desc?}', 'RoomsController@deleted');
	});

	Route::group(['prefix' => 'slides'], function() {
		Route::get('list/{sortBy?}/{desc?}', 'SlidesController@index');
		Route::get('find/{id}', 'SlidesController@find');
		Route::get('search/{query?}/{perPage?}/{sortBy?}/{desc?}', 'SlidesController@search');
		Route::get('paginate/{perPage?}/{sortBy?}/{desc?}', 'SlidesController@paginate');
		Route::get('delete/{id}', 'SlidesController@delete');
		Route::get('restore/{id}', 'SlidesController@restore');
		Route::post('first', 'SlidesController@first');
		Route::post('findby/{sortBy?}/{desc?}', 'SlidesController@findby');
		Route::post('paginateby/{perPage?}/{sortBy?}/{desc?}', 'SlidesController@paginateby');
		Route::post('save', 'SlidesController@save');
		Route::post('deleted/{perPage?}/{sortBy?}/{desc?}', 'SlidesController@deleted');
	});

	Route::group(['prefix' => 'trips'], function() {
		Route::get('list/{sortBy?}/{desc?}', 'TripsController@index');
		Route::get('find/{id}', 'TripsController@find');
		Route::get('search/{query?}/{perPage?}/{sortBy?}/{desc?}', 'TripsController@search');
		Route::get('paginate/{perPage?}/{sortBy?}/{desc?}', 'TripsController@paginate');
		Route::get('delete/{id}', 'TripsController@delete');
		Route::get('restore/{id}', 'TripsController@restore');
		Route::get('featured/{count?}', 'TripsController@featured');
		Route::get('itineraries', 'TripsController@getItineraries');
		Route::post('first', 'TripsController@first');
		Route::post('findby/{sortBy?}/{desc?}', 'TripsController@findby');
		Route::post('paginateby/{perPage?}/{sortBy?}/{desc?}', 'TripsController@paginateby');
		Route::post('save', 'TripsController@save');
		Route::post('deleted/{perPage?}/{sortBy?}/{desc?}', 'TripsController@deleted');
	});

	Route::group(['prefix' => 'tags'], function() {
		Route::get('list/{sortBy?}/{desc?}', 'TagsController@index');
		Route::get('find/{id}', 'TagsController@find');
		Route::get('search/{query?}/{perPage?}/{sortBy?}/{desc?}', 'TagsController@search');
		Route::get('paginate/{perPage?}/{sortBy?}/{desc?}', 'TagsController@paginate');
		Route::get('delete/{id}', 'TagsController@delete');
		Route::get('restore/{id}', 'TagsController@restore');
		Route::post('first', 'TagsController@first');
		Route::post('findby/{sortBy?}/{desc?}', 'TagsController@findby');
		Route::post('paginateby/{perPage?}/{sortBy?}/{desc?}', 'TagsController@paginateby');
		Route::post('save', 'TagsController@save');
		Route::post('deleted/{perPage?}/{sortBy?}/{desc?}', 'TagsController@deleted');
	});

	Route::group(['prefix' => 'trip/images'], function() {
		Route::get('list/{sortBy?}/{desc?}', 'TripImagesController@index');
		Route::get('find/{id}', 'TripImagesController@find');
		Route::get('search/{query?}/{perPage?}/{sortBy?}/{desc?}', 'TripImagesController@search');
		Route::get('paginate/{perPage?}/{sortBy?}/{desc?}', 'TripImagesController@paginate');
		Route::get('delete/{id}', 'TripImagesController@delete');
		Route::get('restore/{id}', 'TripImagesController@restore');
		Route::post('first', 'TripImagesController@first');
		Route::post('findby/{sortBy?}/{desc?}', 'TripImagesController@findby');
		Route::post('paginateby/{perPage?}/{sortBy?}/{desc?}', 'TripImagesController@paginateby');
		Route::post('save', 'TripImagesController@save');
		Route::post('deleted/{perPage?}/{sortBy?}/{desc?}', 'TripImagesController@deleted');
	});

	Route::group(['prefix' => 'faqs'], function() {
		Route::get('list/{sortBy?}/{desc?}', 'FaqsController@index');
		Route::get('find/{id}', 'FaqsController@find');
		Route::get('search/{query?}/{perPage?}/{sortBy?}/{desc?}', 'FaqsController@search');
		Route::get('paginate/{perPage?}/{sortBy?}/{desc?}', 'FaqsController@paginate');
		Route::get('delete/{id}', 'FaqsController@delete');
		Route::get('restore/{id}', 'FaqsController@restore');
		Route::post('first', 'FaqsController@first');
		Route::post('findby/{sortBy?}/{desc?}', 'FaqsController@findby');
		Route::post('paginateby/{perPage?}/{sortBy?}/{desc?}', 'FaqsController@paginateby');
		Route::post('save', 'FaqsController@save');
		Route::post('deleted/{perPage?}/{sortBy?}/{desc?}', 'FaqsController@deleted');
	});

	Route::group(['prefix' => 'nationalities'], function() {
		Route::get('list/{sortBy?}/{desc?}', 'NationalitiesController@index');
		Route::get('find/{id}', 'NationalitiesController@find');
		Route::get('search/{query?}/{perPage?}/{sortBy?}/{desc?}', 'NationalitiesController@search');
		Route::get('paginate/{perPage?}/{sortBy?}/{desc?}', 'NationalitiesController@paginate');
		Route::get('delete/{id}', 'NationalitiesController@delete');
		Route::get('restore/{id}', 'NationalitiesController@restore');
		Route::post('first', 'NationalitiesController@first');
		Route::post('findby/{sortBy?}/{desc?}', 'NationalitiesController@findby');
		Route::post('paginateby/{perPage?}/{sortBy?}/{desc?}', 'NationalitiesController@paginateby');
		Route::post('save', 'NationalitiesController@save');
		Route::post('deleted/{perPage?}/{sortBy?}/{desc?}', 'NationalitiesController@deleted');
	});

	Route::group(['prefix' => 'tour_languages'], function() {
		Route::get('list/{sortBy?}/{desc?}', 'TourLanguagesController@index');
		Route::get('find/{id}', 'TourLanguagesController@find');
		Route::get('search/{query?}/{perPage?}/{sortBy?}/{desc?}', 'TourLanguagesController@search');
		Route::get('paginate/{perPage?}/{sortBy?}/{desc?}', 'TourLanguagesController@paginate');
		Route::get('delete/{id}', 'TourLanguagesController@delete');
		Route::get('restore/{id}', 'TourLanguagesController@restore');
		Route::post('first', 'TourLanguagesController@first');
		Route::post('findby/{sortBy?}/{desc?}', 'TourLanguagesController@findby');
		Route::post('paginateby/{perPage?}/{sortBy?}/{desc?}', 'TourLanguagesController@paginateby');
		Route::post('save', 'TourLanguagesController@save');
		Route::post('deleted/{perPage?}/{sortBy?}/{desc?}', 'TourLanguagesController@deleted');
	});

	Route::group(['prefix' => 'trip/reservations'], function() {
		Route::get('list/{sortBy?}/{desc?}', 'TripReservationsController@index');
		Route::get('find/{id}', 'TripReservationsController@find');
		Route::get('search/{query?}/{perPage?}/{sortBy?}/{desc?}', 'TripReservationsController@search');
		Route::get('paginate/{perPage?}/{sortBy?}/{desc?}', 'TripReservationsController@paginate');
		Route::get('seen/{id}', 'TripReservationsController@seen');
		Route::post('first', 'TripReservationsController@first');
		Route::post('findby/{sortBy?}/{desc?}', 'TripReservationsController@findby');
		Route::post('paginateby/{perPage?}/{sortBy?}/{desc?}', 'TripReservationsController@paginateby');
		Route::post('reserve', 'TripReservationsController@reserve');
	});

	Route::group(['prefix' => 'contact'], function() {
		Route::post('send', 'ContactController@send');
		Route::post('enquire', 'ContactController@enquire');
	});

	Route::group(['prefix' => 'media'], function() {
		Route::post('upload', 'MediaController@uploadImage');
		Route::post('upload/base64', 'MediaController@uploadImageBas64');
		Route::post('delete', 'MediaController@deleteImage');
	});
	
});