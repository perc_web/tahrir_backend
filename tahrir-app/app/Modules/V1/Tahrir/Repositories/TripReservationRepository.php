<?php namespace App\Modules\V1\Tahrir\Repositories;

use App\Modules\V1\Core\AbstractRepositories\AbstractRepository;

class TripReservationRepository extends AbstractRepository
{
	/**
	 * Return the model full namespace.
	 * 
	 * @return string
	 */
	protected function getModel()
	{
		return 'App\Modules\V1\Tahrir\TripReservation';
	}
    
    /**
     * Mark the reserevation as seen.
     * 
     * @param  integer $id
     * @return void
     */
    public function seen($id)
    {
        return $this->save([
			'id'   => $id,
			'seen' => 1
        ]);
    }
}
