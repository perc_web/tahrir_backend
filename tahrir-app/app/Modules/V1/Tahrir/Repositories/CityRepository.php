<?php namespace App\Modules\V1\Tahrir\Repositories;

use App\Modules\V1\Core\AbstractRepositories\AbstractRepository;

class CityRepository extends AbstractRepository
{
	/**
	 * Return the model full namespace.
	 * 
	 * @return string
	 */
	protected function getModel()
	{
		return 'App\Modules\V1\Tahrir\City';
	}
    
    /**
     * Return home page cities in random order.
     * 
     * @param  integer $count Number of rows default 3.
     * @return colleaction
     */
    public function homePage($count = 15, $relations = [])
    {
        return $this->model->where('show_in_home_page', 1)->with($relations)->inRandomOrder()->take($count)->get();
    }
}
