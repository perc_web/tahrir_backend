<?php namespace App\Modules\V1\Tahrir\Repositories;

use App\Modules\V1\Core\AbstractRepositories\AbstractRepository;

class TripRepository extends AbstractRepository
{
	/**
	 * Return the model full namespace.
	 * 
	 * @return string
	 */
	protected function getModel()
	{
		return 'App\Modules\V1\Tahrir\Trip';
	}

    /**
     * Save the given model to the storage.
     * 
     * @param  array   $data
     * @param  boolean $saveLog
     * @return void
     */
    public function save(array $data, $saveLog = true)
    {
    	$model = parent::save($data, $saveLog);

    	$model->rooms()->detach();
    	$model->related()->detach();

    	if (isset($data['related'])) 
    	{
    		foreach ($data['related'] as $related) 
    		{
    			$model->related()->attach($related['id']);
    		}
    	}
    	
    	if (isset($data['has_accommodation']) && $data['has_accommodation']) 
    	{
	    	foreach ($data['hotels'] as $hotel) 
	    	{
	    		foreach ($hotel['rooms'] as $room) 
	    		{
	    			$model->rooms()->attach($room['id'], [
						'hotel_id' => $hotel['id'],
						'price'    => $room['price'],
						'count'    => $room['count']
	    			]);
	    		}
	    	}
    	}
    }
    
    /**
     * Return featured trips in random order.
     * 
     * @param  integer $count Number of rows default 3.
     * @return colleaction
     */
    public function featured($count = 15, $relations = [])
    {
        return $this->model->where('featured', 1)->with($relations)->inRandomOrder()->take($count)->get();
    }

    /**
     * Get all avaialbel itineraries;
     * 
     * @return collection
     */
    public function getItineraries()
    {
        return $this->model->distinct()->get(['number_of_itineraries'])->sort()->pluck('number_of_itineraries');
    }
}
