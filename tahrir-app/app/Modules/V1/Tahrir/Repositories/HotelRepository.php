<?php namespace App\Modules\V1\Tahrir\Repositories;

use App\Modules\V1\Core\AbstractRepositories\AbstractRepository;

class HotelRepository extends AbstractRepository
{
	/**
	 * Return the model full namespace.
	 * 
	 * @return string
	 */
	protected function getModel()
	{
		return 'App\Modules\V1\Tahrir\Hotel';
	}
}
