<?php namespace App\Modules\V1\Tahrir;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TripReservation extends Model{

    use SoftDeletes;
    protected $table    = 'trip_reservations';
    protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden   = ['deleted_at'];
    protected $guarded  = ['id'];
    protected $fillable = ['price', 'name', 'email', 'phone', 'nationality', 'description', 'tour_language', 'from', 'reservation_details', 'state', 'accommodation', 'number_of_adults', 'number_of_childs_2_6', 'number_of_childs_6_12', 'baby_chair', 'childs', 'seen', 'should_pay', 'trip_id'];
    public $searchable  = ['price', 'name', 'email', 'phone', 'nationality', 'description', 'tour_language', 'state'];
    public $casts       = ['reservation_details' => 'json'];

    /**
     * Add time zone diff to created_at
     * 
     * @param  date $value
     * @return date
     */
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get(\CoreConfig::getConfig()['var_names']['timeZoneDiff']))->toDateTimeString();
    }

    /**
     * Add time zone diff to updated_at
     * 
     * @param  date $value
     * @return date
     */
    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get(\CoreConfig::getConfig()['var_names']['timeZoneDiff']))->toDateTimeString();
    }

    /**
     * Add time zone diff to deleted_at
     * 
     * @param  date $value
     * @return date
     */
    public function getDeletedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get(\CoreConfig::getConfig()['var_names']['timeZoneDiff']))->toDateTimeString();
    }

    /**
     * Return the trip.
     * 
     * @return colection
     */
    public function trip()
    {
        return $this->belongsTo('App\Modules\V1\Tahrir\Trip');
    }

    public static function boot()
    {
        parent::boot();
        parent::observe(\App::make('App\Modules\V1\Tahrir\ModelObservers\TripReservationObserver'));
    }
}
