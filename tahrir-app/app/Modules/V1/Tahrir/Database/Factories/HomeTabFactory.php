<?php

$factory->define(App\Modules\V1\Tahrir\HomeTab::class, function (Faker\Generator $faker) {
    return [
    	'tab_name'    => $faker->randomElement(['tours', 'booking', 'deals']),
		'description' => [
			'en' => $faker->sentence(600, true),
			'sp' => $faker->sentence(600, true),
			'fr' => $faker->sentence(600, true),
		],
		'created_at'  => $faker->dateTimeBetween('-1 years', 'now'),
		'updated_at'  => $faker->dateTimeBetween('-1 years', 'now')
    ];
});