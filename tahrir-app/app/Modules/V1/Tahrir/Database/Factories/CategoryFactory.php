<?php

$factory->define(App\Modules\V1\Tahrir\Category::class, function (Faker\Generator $faker) {
    return [
		'image'      => 'http://lorempixel.com/100/100/',
		'name'       => [
			'en' => $faker->word(),
			'sp' => $faker->word(),
			'fr' => $faker->word(),
		],
		'created_at' => $faker->dateTimeBetween('-1 years', 'now'),
		'updated_at' => $faker->dateTimeBetween('-1 years', 'now')
    ];
});
