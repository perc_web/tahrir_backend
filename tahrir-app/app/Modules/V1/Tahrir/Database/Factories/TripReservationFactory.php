<?php

$factory->define(App\Modules\V1\Tahrir\TripReservation::class, function (Faker\Generator $faker) {
    return [
		'price'               => $faker->randomFloat(),
		'name'                => $faker->name(),
		'email'               => $faker->safeEmail(),
		'telephone'           => $faker->e164PhoneNumber(),
		'nationality'         => $faker->word(),
		'description'         => $faker->word(),
		'tour_language'       => $faker->word(),
		'from'                => $faker->dateTimeBetween('-1 years', 'now'),
		'reservation_details' => [
			'number_of_adults' => $faker->randomDigit(),
			'number_of_childs' => $faker->randomDigit()
		],
		'state'           	  => $faker->randomElement(['pending', 'complete', 'canceled']),
		'trip_id'             => $faker->randomDigit(),
		'created_at'          => $faker->dateTimeBetween('-1 years', 'now'),
		'updated_at'          => $faker->dateTimeBetween('-1 years', 'now')
    ];
});