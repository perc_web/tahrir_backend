<?php

$factory->define(App\Modules\V1\Tahrir\Room::class, function (Faker\Generator $faker) {
    return [
		'type'       => [
			'en' => $faker->randomElement(['Single', 'Double', 'Triple']),
			'sp' => $faker->randomElement(['Single_sp', 'Double_sp', 'Triple_sp']),
			'fr' => $faker->randomElement(['Single_fr', 'Double', 'Triple_fr']),
		],
		'created_at' => $faker->dateTimeBetween('-1 years', 'now'),
		'updated_at' => $faker->dateTimeBetween('-1 years', 'now')
    ];
});
