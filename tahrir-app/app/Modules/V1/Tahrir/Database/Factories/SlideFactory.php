<?php

$factory->define(App\Modules\V1\Tahrir\Slide::class, function (Faker\Generator $faker) {
    return [
		'image'             => 'http://lorempixel.com/100/100/',
		'trip_notes' => [
			'en' => $faker->word(),
			'sp' => $faker->word(),
			'fr' => $faker->word(),
		],
		'short_description' => [
			'en' => $faker->word(),
			'sp' => $faker->word(),
			'fr' => $faker->word(),
		],
		'created_at'        => $faker->dateTimeBetween('-1 years', 'now'),
		'updated_at'        => $faker->dateTimeBetween('-1 years', 'now')
    ];
});
