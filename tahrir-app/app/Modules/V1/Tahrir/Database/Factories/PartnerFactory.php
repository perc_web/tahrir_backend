<?php

$factory->define(App\Modules\V1\Tahrir\Partner::class, function (Faker\Generator $faker) {
    return [
		'image'      => 'http://lorempixel.com/100/100/',
		'hide'       => $faker->randomElement([0, 1]),
		'position'   => $faker->randomElement([0, 1]),
		'created_at' => $faker->dateTimeBetween('-1 years', 'now'),
		'updated_at' => $faker->dateTimeBetween('-1 years', 'now')
    ];
});
