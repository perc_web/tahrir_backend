<?php

$factory->define(App\Modules\V1\Tahrir\City::class, function (Faker\Generator $faker) {
    return [
		'image'      => 'http://lorempixel.com/100/100/',
		'name'       => [
			'en' => $faker->state(),
			'sp' => $faker->state(),
			'fr' => $faker->state(),
		],
		'created_at' => $faker->dateTimeBetween('-1 years', 'now'),
		'updated_at' => $faker->dateTimeBetween('-1 years', 'now')
    ];
});
