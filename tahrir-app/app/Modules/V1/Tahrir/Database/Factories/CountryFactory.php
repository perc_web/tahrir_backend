<?php

$factory->define(App\Modules\V1\Tahrir\Country::class, function (Faker\Generator $faker) {
    return [
		'image'      => 'http://lorempixel.com/100/100/',
		'name'       => [
			'en' => $faker->country(),
			'sp' => $faker->country(),
			'fr' => $faker->country(),
		],
		'created_at' => $faker->dateTimeBetween('-1 years', 'now'),
		'updated_at' => $faker->dateTimeBetween('-1 years', 'now')
    ];
});
