<?php

$factory->define(App\Modules\V1\Tahrir\Trip::class, function (Faker\Generator $faker) {
    return [
		'image' => 'http://lorempixel.com/100/100/',
		'title' => [
			'en' => $faker->word(),
			'sp' => $faker->word(),
			'fr' => $faker->word(),
		],
		'run' => [
			'en' => $faker->word(),
			'sp' => $faker->word(),
			'fr' => $faker->word(),
		],
		'duration' => [
			'en' => $faker->word(),
			'sp' => $faker->word(),
			'fr' => $faker->word(),
		],
		'overview' => [
			'en' => $faker->sentence(600, true),
			'sp' => $faker->sentence(600, true),
			'fr' => $faker->sentence(600, true),
		],
		'short_description' => [
			'en' => $faker->sentence(300, true),
			'sp' => $faker->sentence(300, true),
			'fr' => $faker->sentence(300, true),
		],
		'include' => [
			'en' => $faker->sentence(600, true),
			'sp' => $faker->sentence(600, true),
			'fr' => $faker->sentence(600, true),
		],
		'exclude' => [
			'en' => $faker->sentence(600, true),
			'sp' => $faker->sentence(600, true),
			'fr' => $faker->sentence(600, true),
		],
		'program' => [
			'en' => [
				['image' => 'http://lorempixel.com/100/100/', 'title' => $faker->word(), 'description' => $faker->sentence(600, true)],
				['image' => 'http://lorempixel.com/100/100/', 'title' => $faker->word(), 'description' => $faker->sentence(600, true)],
				['image' => 'http://lorempixel.com/100/100/', 'title' => $faker->word(), 'description' => $faker->sentence(600, true)]
			],
			'sp' => [
				['image' => 'http://lorempixel.com/100/100/', 'title' => $faker->word(), 'description' => $faker->sentence(600, true)],
				['image' => 'http://lorempixel.com/100/100/', 'title' => $faker->word(), 'description' => $faker->sentence(600, true)],
				['image' => 'http://lorempixel.com/100/100/', 'title' => $faker->word(), 'description' => $faker->sentence(600, true)]
			],
			'fr' => [
				['image' => 'http://lorempixel.com/100/100/', 'title' => $faker->word(), 'description' => $faker->sentence(600, true)],
				['image' => 'http://lorempixel.com/100/100/', 'title' => $faker->word(), 'description' => $faker->sentence(600, true)],
				['image' => 'http://lorempixel.com/100/100/', 'title' => $faker->word(), 'description' => $faker->sentence(600, true)]
			],
		],
		'prices' => [
			'en' => [
				['size' => $faker->randomNumber(), 'price' => $faker->randomDigit()],
				['size' => $faker->randomNumber(), 'price' => $faker->randomDigit()],
				['size' => $faker->randomNumber(), 'price' => $faker->randomDigit()]
			],
			'sp' => [
				['size' => $faker->randomNumber(), 'price' => $faker->randomDigit()],
				['size' => $faker->randomNumber(), 'price' => $faker->randomDigit()],
				['size' => $faker->randomNumber(), 'price' => $faker->randomDigit()]
			],
			'fr' => [
				['size' => $faker->randomNumber(), 'price' => $faker->randomDigit()],
				['size' => $faker->randomNumber(), 'price' => $faker->randomDigit()],
				['size' => $faker->randomNumber(), 'price' => $faker->randomDigit()]
			],
		],
		'cancellation_policy' => [
			'en' => $faker->sentence(600, true),
			'sp' => $faker->sentence(600, true),
			'fr' => $faker->sentence(600, true),
		],
		'payment' => [
			'en' => $faker->sentence(600, true),
			'sp' => $faker->sentence(600, true),
			'fr' => $faker->sentence(600, true),
		],
		'children_policy' => [
			'en' => $faker->sentence(600, true),
			'sp' => $faker->sentence(600, true),
			'fr' => $faker->sentence(600, true),
		],
		'tour_voucher' => [
			'en' => $faker->sentence(600, true),
			'sp' => $faker->sentence(600, true),
			'fr' => $faker->sentence(600, true),
		],
		'tour_type' => [
			'en' => $faker->sentence(600, true),
			'sp' => $faker->sentence(600, true),
			'fr' => $faker->sentence(600, true),
		],
		'start_point' => [
			'en' => $faker->word(),
			'sp' => $faker->word(),
			'fr' => $faker->word(),
		],
		'pickup_time'       => $faker->time('H:i', 'now'),
		'start_date'        => $faker->dateTimeBetween('now', '+1 days'),
		'end_date'          => $faker->dateTimeBetween('+2 days', '+3 days'),
		'has_accommodation' => $faker->randomElement([0, 1]),
		'featured'          => $faker->randomElement([0, 1]),
		'lowestprice'       => $faker->randomDigit(),
		'created_at'        => $faker->dateTimeBetween('-1 years', 'now'),
		'updated_at'        => $faker->dateTimeBetween('-1 years', 'now')
    ];
});
