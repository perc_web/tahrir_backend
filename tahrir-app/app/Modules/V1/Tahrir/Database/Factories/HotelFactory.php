<?php

$factory->define(App\Modules\V1\Tahrir\Hotel::class, function (Faker\Generator $faker) {
    return [
		'image'      => 'http://lorempixel.com/100/100/',
		'name'       => [
			'en' => $faker->word(),
			'sp' => $faker->word(),
			'fr' => $faker->word(),
		],
    	'stars'      => $faker->randomElement([1, 2, 3, 4, 5]),
		'created_at' => $faker->dateTimeBetween('-1 years', 'now'),
		'updated_at' => $faker->dateTimeBetween('-1 years', 'now')
    ];
});
