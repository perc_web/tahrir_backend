<?php

$factory->define(App\Modules\V1\Tahrir\TourLanguage::class, function (Faker\Generator $faker) {
    return [
		'name'       => [
			'en' => $faker->word(),
			'sp' => $faker->word(),
			'fr' => $faker->word(),
		],
		'created_at' => $faker->dateTimeBetween('-1 years', 'now'),
		'updated_at' => $faker->dateTimeBetween('-1 years', 'now')
    ];
});
