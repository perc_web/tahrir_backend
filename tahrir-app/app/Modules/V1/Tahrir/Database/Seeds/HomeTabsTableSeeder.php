<?php

namespace App\Modules\V1\Tahrir\Database\Seeds;

use Illuminate\Database\Seeder;

class HomeTabsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/**
         * Insert the permissions related to homeTabs table.
         */
        \DB::table('permissions')->insert(
        	[
        		/**
        		 * homeTabs model permissions.
        		 */
	        	[
	        	'name'       => 'save',
	        	'model'      => 'homeTabs',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
	        	[
	        	'name'       => 'delete',
	        	'model'      => 'homeTabs',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
	        	[
	        	'name'       => 'find',
	        	'model'      => 'homeTabs',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
	        	[
	        	'name'       => 'list',
	        	'model'      => 'homeTabs',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
	        	[
	        	'name'       => 'search',
	        	'model'      => 'homeTabs',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
	        	[
	        	'name'       => 'findby',
	        	'model'      => 'homeTabs',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
	        	[
	        	'name'       => 'first',
	        	'model'      => 'homeTabs',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
	        	[
	        	'name'       => 'paginate',
	        	'model'      => 'homeTabs',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
	        	[
	        	'name'       => 'paginateby',
	        	'model'      => 'homeTabs',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
                [
                'name'       => 'deleted',
                'model'      => 'homeTabs',
                'created_at' => \DB::raw('NOW()'),
                'updated_at' => \DB::raw('NOW()')
                ],
                [
                'name'       => 'restore',
                'model'      => 'homeTabs',
                'created_at' => \DB::raw('NOW()'),
                'updated_at' => \DB::raw('NOW()')
                ]
        	]
        );

		/**
		 * Create Default tabs.
		 */
		/*\DB::table('home_taps')->insert(
			[
				[
					'tab_name'    => 'tours',
					'description' => '{"en": "", "sp": "", "fr": ""}',
					'created_at'  => \DB::raw('NOW()'),
					'updated_at'  => \DB::raw('NOW()')
				],
				[
					'tab_name'    => 'booking',
					'description' => '{"en": "", "sp": "", "fr": ""}',
					'created_at'  => \DB::raw('NOW()'),
					'updated_at'  => \DB::raw('NOW()')
				],
				[
					'tab_name'    => 'deals',
					'description' => '{"en": "", "sp": "", "fr": ""}',
					'created_at'  => \DB::raw('NOW()'),
					'updated_at'  => \DB::raw('NOW()')
				]
			]
		);*/
    }
}
