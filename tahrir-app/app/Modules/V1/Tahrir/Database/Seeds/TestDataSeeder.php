<?php

namespace App\Modules\V1\Tahrir\Database\Seeds;

use Illuminate\Database\Seeder;

class TestDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory('App\Modules\V1\Tahrir\Slide', 20)->create();
        factory('App\Modules\V1\Tahrir\Partner', 20)->create();
        factory('App\Modules\V1\Tahrir\HomeTab', 20)->create();
        factory('App\Modules\V1\Tahrir\Category', 20)->create();
        factory('App\Modules\V1\Tahrir\Tag', 20)->create();
        factory('App\Modules\V1\Tahrir\Country', 20)->create();
        factory('App\Modules\V1\Tahrir\Room', 20)->create();
        factory('App\Modules\V1\Tahrir\Faq', 20)->create();
        factory('App\Modules\V1\Tahrir\City', 20)->make()->each(function ($city) {
            $city->country_id = \App\Modules\V1\Tahrir\Country::all()->random(1)->first()->id;
            $city->save();
        });
        factory('App\Modules\V1\Tahrir\Hotel', 20)->create()->each(function ($hotel) {
            $hotel->rooms()->attach(\App\Modules\V1\Tahrir\Room::all()->random(5));
        });
        factory('App\Modules\V1\Tahrir\Trip', 20)->make()->each(function ($trip) {
            $faker            = \Faker\Factory::create();
            $trip->country_id = \App\Modules\V1\Tahrir\Country::all()->random(1)->first()->id;
            $trip->city_id    = \App\Modules\V1\Tahrir\City::all()->random(1)->first()->id;
            $trip->save();

            $trip->categories()->attach(\App\Modules\V1\Tahrir\Category::all()->random(5));
            $trip->tags()->attach(\App\Modules\V1\Tahrir\Tag::all()->random(5));
            $trip->related()->attach(\App\Modules\V1\Tahrir\Trip::all()->random(1));
            if ($trip->has_accommodation) 
            {
                \App\Modules\V1\Tahrir\Room::all()->random(5)->each(function($room) use ($trip, $faker){
                    $trip->rooms()->attach($room, [
                        'hotel_id'  => $room->hotels()->get()->random(1)->first()->id,
                        'price'     => $faker->randomDigit(),
                        'count'     => $faker->randomDigitNotNull(),
                        'reserved'  => 0,
                        'avaialble' => 1,
                    ]);
                });
            }
        });
        factory('App\Modules\V1\Tahrir\TripImage', 20)->make()->each(function ($tripImage) {
            $tripImage->trip_id = \App\Modules\V1\Tahrir\Trip::all()->random(1)->first()->id;
            $tripImage->save();
        });

    }
}
