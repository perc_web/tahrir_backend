<?php

namespace App\Modules\V1\Tahrir\Database\Seeds;

use Illuminate\Database\Seeder;

class TahrirDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ClearDataSeeder::class);
    	$this->call(CountriesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(HomeTabsTableSeeder::class);
        $this->call(HotelsTableSeeder::class);
        $this->call(PartnersTableSeeder::class);
        $this->call(SlidesTableSeeder::class);
        $this->call(TripsTableSeeder::class);
        $this->call(RoomsTableSeeder::class);
        $this->call(TagsTableSeeder::class);
        $this->call(TripImagesTableSeeder::class);
        $this->call(FaqsTableSeeder::class);
        $this->call(NationalitiesTableSeeder::class);
        $this->call(TourLanguagesTableSeeder::class);
        $this->call(TripReservationsTableSeeder::class);
    	$this->call(AssignRelationsSeeder::class);

        /**
         * Testing
         */
        //$this->call(TestDataSeeder::class);
    }
}
