<?php

namespace App\Modules\V1\Tahrir\Database\Seeds;

use Illuminate\Database\Seeder;

class ClearDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions  = \DB::table('permissions')->whereIn('model', [
            'countries',
            'cities',
            'categories',
            'homeTabs',
            'hotels',
            'partners',
            'slides',
            'trips',
            'rooms',
            'tags',
            'tripImages',
            'faqs',
            'nationalities',
            'tourLanguages',
            'tripReservations'
            ]);
        \DB::table('groups_permissions')->whereIn('permission_id', $permissions->pluck('id'))->delete();
        $permissions->delete();
    }
}
