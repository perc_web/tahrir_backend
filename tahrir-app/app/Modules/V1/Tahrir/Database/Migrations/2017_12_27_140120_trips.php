<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Trips extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trips', function (Blueprint $table) {
            $table->increments('id');
            $table->string('image', 150)->nullable();
            $table->json('title');
            $table->json('run');
            $table->json('duration');
            $table->json('overview');
            $table->json('include');
            $table->json('exclude');
            $table->json('program');
            $table->integer('country_id');
            $table->integer('city_id');
            $table->datetime('start_date');
            $table->datetime('end_date');
            $table->boolean('has_accommodation')->default(0);
            $table->boolean('featured')->default(0);
            $table->json('prices');
            $table->json('cancellation_policy');
            $table->json('payment');
            $table->json('children_policy');
            $table->json('tour_voucher');
            $table->json('tour_type');
            $table->json('start_point');
            $table->time('pickup_time');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('trips_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trip_id');
            $table->integer('category_id');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('trips_related', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trip_id');
            $table->integer('related_id');
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::create('trips_rooms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trip_id');
            $table->integer('room_id');
            $table->integer('hotel_id');
            $table->decimal('price', 12, 2)->default(0);
            $table->integer('count')->default(0);
            $table->integer('reserved')->default(0);
            $table->boolean('avaialble')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trips');
        Schema::dropIfExists('trips_categories');
        Schema::dropIfExists('trips_related');
        Schema::dropIfExists('trips_rooms');
    }
}
