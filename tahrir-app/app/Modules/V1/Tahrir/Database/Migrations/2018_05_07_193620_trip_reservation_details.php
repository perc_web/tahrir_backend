<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TripReservationDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('trip_reservations', function (Blueprint $table) {
            $table->integer('number_of_adults')->nullable();
            $table->integer('number_of_childs_2_6')->nullable();
            $table->integer('number_of_childs_6_12')->nullable();
            $table->boolean('baby_chair')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
