<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TripReservations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('price', 12, 2)->default(0);
            $table->string('name');
            $table->string('email');
            $table->string('phone');
            $table->string('nationality');
            $table->string('description')->nullable();
            $table->string('tour_language')->nullable();
            $table->date('from')->nullable();
            $table->json('reservation_details')->nullable();
            $table->enum('state', ['pending', 'complete', 'canceled'])->default('pending');
            $table->integer('trip_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_reservations');
    }
}