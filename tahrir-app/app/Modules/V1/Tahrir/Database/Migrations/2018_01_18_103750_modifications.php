<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Modifications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cities', function (Blueprint $table) {
            $table->boolean('show_in_home_page')->default(0);
        });

        Schema::table('categories', function (Blueprint $table) {
            $table->integer('order')->default(0);
        });

        Schema::table('trips', function (Blueprint $table) {
            $table->decimal('lowest_price', 12, 2)->default(0);
            $table->json('short_description');
            $table->dropColumn(['start_date', 'end_date']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
