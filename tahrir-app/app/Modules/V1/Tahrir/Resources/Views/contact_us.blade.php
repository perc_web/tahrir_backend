Contact message from {{$sender['name']}},
<br><br>

	{{$sender['message']}}

<br><br>	
Sender info:

@foreach($sender as $key => $value)
	@if($key !== 'message' && $key !== 'name')
		<br>
		{{$key}}: {{$value}}
	@endif
@endforeach