<!DOCTYPE html>
<html>
<head>
    <title>Payfort Redirect</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <style type="text/css">
        #payfortpaymentform {
            display:none;
        }
        .center {
            width: 50%;
            margin: 0 auto;
            text-align: center;
        }
        #logo {
            margin-top:50px;
            max-width: 100%;
        }
        .title {
            text-align: center;
            margin-top:50px;
            font-size: 14px;
        }
        .proceed {
            font-weight: 700;
            font-size: 15px;
            color: #fff;
            background-color: #a71d22;
            border: 1px solid #fff;
            border-bottom: 3px solid #fff;
            text-transform: uppercase;
            letter-spacing: 1px;
        }
        .btn-block {
            display: block;
            width: 100%;
        }
        .btn {
            display: inline-block;
            padding: 6px 12px;
            margin-bottom: 0;
            font-size: 14px;
            font-weight: normal;
            line-height: 1.42857143;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -ms-touch-action: manipulation;
            touch-action: manipulation;
            cursor: pointer;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            background-image: none;
            border: 1px solid transparent;
            border-radius: 4px;
        }
    </style>
</head>
<body>
    
    @if(isset($secureUrl))
        <script>window.parent.location = "{{$secureUrl}}";</script>
    @elseIf(isset($redirect))
        <div class="center wrapper">
            <p class="title">{{$message}}</p>
        </div>
        <script>
            (function () {
                setTimeout(function () {
                    window.parent.location = "{{$redirect}}";
                }, 5000);
            })();
        </script>
    @else
        <div class="center wrapper">
            <img src="https://docs.payfort.com/docs/redirection/build/images/logo.png" id="logo">
            <p class="title"><button class="btn btn-block proceed" onclick="document.payfortpaymentform.submit();">Proceed</button></p>
            <form name="payfortpaymentform" id="payfortpaymentform" method="post" action="{{$payfortBaseUrl}}" id="form1" name="form1" target='_parent'>
                <!-- general parameters -->
                @foreach($payfortParams as $key => $value)
                <input type="hidden" name="{{$key}}" value="{{$value}}">
                @endforeach
                <input type="submit" value="" id="submit2" name="submit2">
            </form>
        </div>
    @endif

</body>
</html>

