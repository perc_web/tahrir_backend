<?php namespace App\Modules\V1\Core\Facades;

use Illuminate\Support\Facades\Facade;

class Core extends Facade
{
	protected static function getFacadeAccessor() { return 'Core'; }
}