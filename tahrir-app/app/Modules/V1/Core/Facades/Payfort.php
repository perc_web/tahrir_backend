<?php namespace App\Modules\V1\Core\Facades;

use Illuminate\Support\Facades\Facade;

class Payfort extends Facade
{
	protected static function getFacadeAccessor() { return 'Payfort'; }
}