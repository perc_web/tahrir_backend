<?php namespace App\Modules\V1\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Log extends Model{

    use SoftDeletes;
    protected $table    = 'logs';
    protected $dates    = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden   = ['deleted_at', 'item_type'];
    protected $guarded  = ['id'];
    protected $fillable = ['action', 'item_name', 'item_type', 'item_id', 'user_id'];
    public $searchable  = ['action', 'item_name', 'item_type'];

    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get('timeZoneDiff'))->toDateTimeString();
    }

    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get('timeZoneDiff'))->toDateTimeString();
    }

    public function getDeletedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get('timeZoneDiff'))->toDateTimeString();
    }
    
    public function user()
    {
        return $this->belongsTo('App\Modules\V1\Acl\AclUser');
    }

    public function item()
    {
        return $this->morphTo();
    }

    public static function boot()
    {
        parent::boot();
        parent::observe(\App::make('App\Modules\V1\Core\ModelObservers\LogObserver'));
    }
}
