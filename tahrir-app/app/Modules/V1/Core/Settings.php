<?php namespace App\Modules\V1\Core;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Modules\V1\Tahrir\Traits\Translatable;

class Settings extends Model{

    use SoftDeletes, Translatable;
    protected $table     = 'settings';
    protected $dates     = ['created_at', 'updated_at', 'deleted_at'];
    protected $hidden    = ['deleted_at'];
    protected $guarded   = ['id', 'key'];
    protected $fillable  = ['name','value'];
    public $searchable   = ['name', 'key'];
    public $translatable = ['value'];
    public $casts        = ['value' => 'json'];
    
    public function getCreatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get('timeZoneDiff'))->toDateTimeString();
    }

    public function getUpdatedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get('timeZoneDiff'))->toDateTimeString();
    }

    public function getDeletedAtAttribute($value)
    {
        return \Carbon\Carbon::parse($value)->addHours(\Session::get('timeZoneDiff'))->toDateTimeString();
    }
    
    public function newCollection(array $models = [])
    {
        return parent::newCollection($models)->keyBy('key');
    }

    public static function boot()
    {
        parent::boot();
        parent::observe(\App::make('App\Modules\V1\Core\ModelObservers\SettingsObserver'));
    }
}
