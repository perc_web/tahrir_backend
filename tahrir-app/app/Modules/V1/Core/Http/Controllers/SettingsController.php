<?php
namespace App\Modules\V1\Core\Http\Controllers;

use Illuminate\Foundation\Http\FormRequest;
use App\Modules\V1\Core\Http\Controllers\BaseApiController;
use Illuminate\Http\Request;


class SettingsController extends BaseApiController
{
    /**
     * The name of the model that is used by the base api controller 
     * to preform actions like (add, edit ... etc).
     * @var string
     */
    protected $model               = 'settings';
    
    /**
     * List of all route actions that the base api controller
     * will skip login check for them.
     * @var array
     */
    protected $skipLoginCheck  = ['list'];

    /**
     * The validations rules used by the base api controller
     * to check before add.
     * @var array
     */
    protected $validationRules  = [
    'name'  => 'required|string|max:100',
    'value' => 'required'
    ];


    /**
     * Save list of settings.
     * 
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function saveMany(Request $request) 
    {   
        if ($this->model) 
        {
            return \Response::json(call_user_func_array("\Core::{$this->model}", [])->saveMany($request->all()), 200);
        }
    }
}
