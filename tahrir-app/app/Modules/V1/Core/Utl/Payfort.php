<?php namespace App\Modules\V1\Core\Utl;

class Payfort
{
    /**
     * The accessCode implementation.
     * 
     * @var string
     */
    protected $accessCode;

    /**
     * The merchantIdentifier implementation.
     * 
     * @var string
     */
    protected $merchantIdentifier;

    /**
     * The payfortUrl implementation.
     * 
     * @var string
     */
    protected $payfortBaseUrl;

    /**
     * The requestPharase implementation.
     * 
     * @var string
     */
    protected $requestPharase;

    /**
     * The tahrirFrontUrl implementation.
     * 
     * @var string
     */
    protected $tahrirFrontUrl;
    
    /**
     * The guzzleHttpClient implementation.
     * 
     * @var \GuzzleHttp\Client
     */
    protected $guzzleHttpClient;

    public function __construct()
    {        
        $this->accessCode              = env('PAYFORT_ACCESS_CODE');
        $this->merchantIdentifier      = env('PAYFORT_MERCHANT_IDENTIFIER');
        $this->requestPharase          = env('PAYFORT_REQUEST_PHRASE');
        $this->shaType                 = env('PAYFORT_SHA_TYPE');
        $this->returnUrl               = env('PAYFORT_RETURN_URL');
        $this->payfortBaseUrl          = env('PAYFORT_TESTING') ? env('PAYFORT_TESTING_BASE_URL') : env('PAYFORT_PRODUCTION_BASE_URL');
        $this->payfortOperationBaseUrl = env('PAYFORT_TESTING') ? env('PAYFORT_TESTING_OPERATION_BASE_URL') : env('PAYFORT_PRODUCTION_OPERATION_BASE_URL');
        $this->tahrirFrontUrl          = env('TAHRIR_FRONT_URL');
        $this->guzzleHttpClient        = \App::make('\GuzzleHttp\Client');
    }

    /**
     * Redirect to payfort merchant page.
     * 
     * @param  integer $tripReservationId
     * @param  string  $language
     * @param  string  $tokenName
     * @return void
     */
    public function redirect($tripReservationId, $language, $tokenName) 
    {
        $tripReservation = \Core::tripReservations()->find($tripReservationId);
        $payfortBaseUrl  = $this->payfortBaseUrl;
        $payfortParams   = [
            'command'             => 'PURCHASE',
            'access_code'         => $this->accessCode,
            'merchant_identifier' => $this->merchantIdentifier,
            'merchant_reference'  => $tripReservationId ,
            'amount'              => (float)$tripReservation->price * 100,
            'currency'            => 'USD',
            'language'            => $language,
            'customer_name'       => $tripReservation->name,
            'customer_email'      => $tripReservation->email,
            'return_url'          => $this->returnUrl,
        ];

        $payfortParams['signature'] = $this->calcSignature($payfortParams);
        return view('tahrir::payfort_redirect', compact('payfortParams', 'payfortBaseUrl'));
    }

    /**
     * Handle payfort response.
     * 
     * @param  array $responseGatewayParams
     * @return void
     */
    public function response($responseGatewayParams) 
    {
        switch (array_get($responseGatewayParams, 'status', 00))
        {
            case 18:
                return redirect('payfort/redirect/' . $responseGatewayParams['merchant_reference'] . '/' . $responseGatewayParams['language'] . '/'. $responseGatewayParams['token_name']);
                break;
            case 14:
            case 02:
                if($tripReservation = \Core::tripReservations()->find($responseGatewayParams['merchant_reference'], ['trip']))
                {
                    $tripReservation->state = 'complete';   
                    $tripReservation->save();

                    $settings = \Core::settings()->all();

                    \Mail::send('tahrir::reserve_email', compact('tripReservation', 'settings'), function ($m) use ($tripReservation) {
                        $m->to($tripReservation->email)->subject('Trip reserved');
                    });
                }
                return view('tahrir::payfort_redirect', ['message' => 'Your payment is complete', 'redirect' => $this->tahrirFrontUrl . 'holiday/' . $tripReservation->trip->id]);
                break;
            case 20:
                return view('tahrir::payfort_redirect', ['secureUrl' => $responseGatewayParams['3ds_url']]);
                break;
            default:
                if($tripReservation = \Core::tripReservations()->find(array_get($responseGatewayParams, 'merchant_reference', 0)))
                {
                    $tripReservation->state = 'canceled';   
                    $tripReservation->save();

                    return view('tahrir::payfort_redirect', ['message' => 'There has been a problem in your payment', 'redirect' => $this->tahrirFrontUrl . 'holiday/' . $tripReservation->trip->id]);
                }
                return view('tahrir::payfort_redirect', ['message' => 'There has been a problem in your payment', 'redirect' => $this->tahrirFrontUrl . 'holiday']);
                break;
        }
    }

    /**
     * Calculate the signature.
     * 
     * @param  array $payfortParams
     * @return void
     */
    public function calcSignature($payfortParams) 
    {
        ksort($payfortParams);
        $shaString = $this->requestPharase . '';
        foreach ($payfortParams as $key => $value) 
        {
            $shaString .= $key . '=' . $value;
        }
        $shaString .= $this->requestPharase;

        return hash($this->shaType, $shaString);
    }
}