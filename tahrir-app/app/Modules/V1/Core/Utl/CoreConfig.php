<?php namespace App\Modules\V1\Core\Utl;
use \App\Modules\V1\Core\Settings;

class CoreConfig
{
    public function getConfig()
    {
        return [
        	'resetLink' => '{{link_here}}',
			'var_names' => [
				'locale'          => 'locale',
				'locale_fallback' => 'en',
				'timeZoneDiff'    => 'timeZoneDiff'
        	],
			/**
			 * Specify what relations should be used for every model.
			 */
			'relations' => [
				'users' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'account'    => [],
					'group'      => [],
					'deleted'    => [],
				],
				'permissions' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'deleted'    => [],
				],
				'groups' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'deleted'    => [],
				],
				'logs' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'deleted'    => [],
				],
				'notifications' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'deleted'    => [],
				],
				'pushNotificationDevices' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'deleted'    => [],
				],
				'reports' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'deleted'    => [],
				],
				'settings' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'deleted'    => [],
				],
				'countries' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'deleted'    => [],
				],
				'cities' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'deleted'    => [],
					'random'     => [],
				],
				'categories' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'deleted'    => [],
				],
				'homeTabs' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'deleted'    => [],
				],
				'hotels' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => ['rooms'],
					'paginateby' => [],
					'first'      => [],
					'search'     => ['rooms'],
					'deleted'    => [],
				],
				'partners' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'deleted'    => [],
				],
				'rooms' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'deleted'    => [],
				],
				'slides' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'deleted'    => [],
				],
				'trips' => [
					'list'       => [],
					'find'       => ['related', '_hotels', 'rooms', 'categories', 'tags', 'tripImages'],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => ['related', 'tripImages', 'categories'],
					'first'      => [],
					'search'     => ['related', 'tripImages', 'categories'],
					'deleted'    => [],
					'featured'   => ['categories'],
				],
				'tags' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'deleted'    => [],
				],
				'tripImages' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => [],
					'paginateby' => [],
					'first'      => [],
					'search'     => [],
					'deleted'    => [],
				],
				'tripReservations' => [
					'list'       => [],
					'find'       => [],
					'findby'     => [],
					'paginate'   => ['trip'],
					'paginateby' => ['trip'],
					'first'      => [],
					'search'     => ['trip'],
					'deleted'    => [],
					'featured'   => [],
				],
			],
			/**
			 * Specify caching config for each api.
			 */
			'cacheConfig' => [
				'countries' => [
					'cache' => [
						'all',
						'find',
						'findBy',
						'paginate',
						'paginateBy',
						'first',
						'search',
						'deleted'
					],
					'clear' => [
						'update'  => ['countries', 'cities', 'trips'],
						'save'    => ['countries', 'cities', 'trips'],
						'delete'  => ['countries', 'cities', 'trips'],
						'restore' => ['countries', 'cities', 'trips'],
					]
				],
				'cities' => [
					'cache' => [
						'all',
						'find',
						'findBy',
						'paginate',
						'paginateBy',
						'first',
						'search',
						'deleted'
					],
					'clear' => [
						'update'  => ['countries', 'cities', 'trips'],
						'save'    => ['countries', 'cities', 'trips'],
						'delete'  => ['countries', 'cities', 'trips'],
						'restore' => ['countries', 'cities', 'trips'],
					]
				],
				'categories' => [
					'cache' => [
						'all',
						'find',
						'findBy',
						'paginate',
						'paginateBy',
						'first',
						'search',
						'deleted'
					],
					'clear' => [
						'update'  => ['categories', 'trips'],
						'save'    => ['categories', 'trips'],
						'delete'  => ['categories', 'trips'],
						'restore' => ['categories', 'trips'],
					]
				],
				'homeTabs' => [
					'cache' => [
						'all',
						'find',
						'findBy',
						'paginate',
						'paginateBy',
						'first',
						'search',
						'deleted'
					],
					'clear' => [
						'update'  => ['homeTabs'],
						'save'    => ['homeTabs'],
						'delete'  => ['homeTabs'],
						'restore' => ['homeTabs'],
					]
				],
				'partners' => [
					'cache' => [
						'all',
						'find',
						'findBy',
						'paginate',
						'paginateBy',
						'first',
						'search',
						'deleted'
					],
					'clear' => [
						'update'  => ['partners'],
						'save'    => ['partners'],
						'delete'  => ['partners'],
						'restore' => ['partners'],
					]
				],
				'slides' => [
					'cache' => [
						'all',
						'find',
						'findBy',
						'paginate',
						'paginateBy',
						'first',
						'search',
						'deleted'
					],
					'clear' => [
						'update'  => ['slides'],
						'save'    => ['slides'],
						'delete'  => ['slides'],
						'restore' => ['slides'],
					]
				],
				'tags' => [
					'cache' => [
						'all',
						'find',
						'findBy',
						'paginate',
						'paginateBy',
						'first',
						'search',
						'deleted'
					],
					'clear' => [
						'update'  => ['tags', 'trips'],
						'save'    => ['tags', 'trips'],
						'delete'  => ['tags', 'trips'],
						'restore' => ['tags', 'trips'],
					]
				],
				'settings' => [
					'cache' => [
						'all',
						'find',
						'findBy',
						'paginate',
						'paginateBy',
						'first',
						'search',
						'deleted'
					],
					'clear' => [
						'update'   => ['settings'],
						'save'     => ['settings'],
						'saveMany' => ['settings']
					]
				],
			]
		];
    }
}