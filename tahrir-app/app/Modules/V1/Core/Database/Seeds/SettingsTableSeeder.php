<?php

namespace App\Modules\V1\Core\Database\Seeds;

use Illuminate\Database\Seeder;

class SettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	/**
         * Insert the permissions related to settings table.
         */
        \DB::table('permissions')->insert(
        	[
        		/**
        		 * Settings model permissions.
        		 */
	        	[
	        	'name'       => 'save',
	        	'model'      => 'settings',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
	        	[
	        	'name'       => 'find',
	        	'model'      => 'settings',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
	        	[
	        	'name'       => 'search',
	        	'model'      => 'settings',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
	        	[
	        	'name'       => 'list',
	        	'model'      => 'settings',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
	        	[
	        	'name'       => 'findby',
	        	'model'      => 'settings',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
	        	[
	        	'name'       => 'first',
	        	'model'      => 'settings',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
	        	[
	        	'name'       => 'paginate',
	        	'model'      => 'settings',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
	        	[
	        	'name'       => 'paginateby',
	        	'model'      => 'settings',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	],
	        	[
	        	'name'       => 'saveMany',
	        	'model'      => 'settings',
	        	'created_at' => \DB::raw('NOW()'),
	        	'updated_at' => \DB::raw('NOW()')
	        	]
        	]
        );

		/**
		 * Create Default settings.
		 */
		\DB::table('settings')->insert(
			[
				[
					'name'       => 'About',
					'key'        => 'about',
					'value'      => '{"en": "", "sp":"", "fr": ""}',
					'created_at' => \DB::raw('NOW()'),
					'updated_at' => \DB::raw('NOW()')
				],
				[
					'name'       => 'Address',
					'key'        => 'address',
					'value'      => '{"en": "", "sp": "", "fr": ""}',
					'created_at' => \DB::raw('NOW()'),
					'updated_at' => \DB::raw('NOW()')
				],
				[
					'name'       => 'Telephones',
					'key'        => 'telephones',
					'value'      => '[]',
					'created_at' => \DB::raw('NOW()'),
					'updated_at' => \DB::raw('NOW()')
				],
				[
					'name'       => 'Primary Email',
					'key'        => 'primary_email',
					'value'      => '[]',
					'created_at' => \DB::raw('NOW()'),
					'updated_at' => \DB::raw('NOW()')
				],
				[
					'name'       => 'Contact Emails',
					'key'        => 'contact_emails',
					'value'      => '["inbound@tahrir-tours.com", "a.khatab@tahrir-tours.com"]',
					'created_at' => \DB::raw('NOW()'),
					'updated_at' => \DB::raw('NOW()')
				],
				[
					'name'       => 'Enquiry Emails',
					'key'        => 'enquiry_emails',
					'value'      => '["inbound@tahrir-tours.com", "a.khatab@tahrir-tours.com"]',
					'created_at' => \DB::raw('NOW()'),
					'updated_at' => \DB::raw('NOW()')
				],
				[
					'name'       => 'Package Page Images',
					'key'        => 'package_page_images',
					'value'      => '[]',
					'created_at' => \DB::raw('NOW()'),
					'updated_at' => \DB::raw('NOW()')
				],
				[
					'name'       => 'Social links',
					'key'        => 'social_links',
					'value'      => '{"facebook":""}',
					'created_at' => \DB::raw('NOW()'),
					'updated_at' => \DB::raw('NOW()')
				],
				[
					'name'       => 'Faqs Page',
					'key'        => 'faqs_page',
					'value'      => '{"image":"" , "content": {"en": "", "sp": "", "fr": ""}}',
					'created_at' => \DB::raw('NOW()'),
					'updated_at' => \DB::raw('NOW()')
				],
				[
					'name'       => 'Contact Page',
					'key'        => 'contact_page',
					'value'      => '{"image":"" , "content": {"en": "", "sp": "", "fr": ""}}',
					'created_at' => \DB::raw('NOW()'),
					'updated_at' => \DB::raw('NOW()')
				],
				[
					'name'       => 'Privacy Page',
					'key'        => 'privacy_page',
					'value'      => '{"image":"" , "content": {"en": "", "sp": "", "fr": ""}}',
					'created_at' => \DB::raw('NOW()'),
					'updated_at' => \DB::raw('NOW()')
				],
				[
					'name'       => 'Terms Page',
					'key'        => 'terms_page',
					'value'      => '{"image":"" , "content": {"en": "", "sp": "", "fr": ""}}',
					'created_at' => \DB::raw('NOW()'),
					'updated_at' => \DB::raw('NOW()')
				]
			]
		);
    }
}
