<?php

return [
    
    /**
     * List of error messages used in core module.
     */
    'unAuthorized'           => 'Please login before any action',
    'tokenExpired'           => 'Login token expired',
    'noPermissions'          => 'No permissions',
    'loginFailed'            => 'Wrong mail or password',
    'noSocialEmail'          => 'Couldn\'t retrieve email',
    'userAlreadyRegistered'  => 'User already registered. Please login using email and password',
    'connectionError'        => 'Connection error',
    'redisNotRunning'        => 'Your redis notification server is\'t running',
    'dbQueryError'           => 'Please check the given inputes',
    'cannotCreateSetting'    => 'Can\'t create setting',
    'cannotUpdateSettingKey' => 'Can\'t update setting key',
    'userIsBlocked'          => 'You have been blocked',
    'invalidResetToken'      => 'Reset password token is invalid',
    'invalidResetPassword'   => 'Reset password is invalid',
    'invalidOldPassword'     => 'Old password is invalid',
    'notFound'               => 'The requested :replace not found',
    'generalError'           => 'Something went wrong',

];