<?php

return [
    
    /**
     * List of error messages used in core module.
     */
    'unAuthorized'           => 'من فضلك قم بتسجيل الدخول',
    'tokenExpired'           => 'انتهت صلاحية الدخول',
    'noPermissions'          => 'لا توجد صلاحية',
    'loginFailed'            => 'خطأ في البريد لاكتروني او كلمة المرور',
    'noSocialEmail'          => 'لا يمكن الحصول علي تابريد الاكتروني',
    'userAlreadyRegistered'  => 'المستخد مسجل بالفعل.سجل الدخول بالبريد الاكتروني و كلمة السر',
    'connectionError'        => 'خطأ في الاتصال',
    'redisNotRunning'        => 'سيرفير الاشعارات لايعمل',
    'dbQueryError'           => 'خطا في البيانات',
    'cannotCreateSetting'    => 'لا يمكن اضافة اعدادات',
    'cannotUpdateSettingKey' => 'لا يمكن تعديل اعدادات',
    'userIsBlocked'          => 'لقد تم حظرك',
    'invalidResetToken'      => 'رمز تعديل كلمة المرور خطا',
    'invalidResetPassword'   => 'خطا في نعديل كلمة المرور',
    'invalidOldPassword'     => 'كلمة السر القديمه خطا',
    'notFound'               => 'ال :replace المطلوب غير موجود',
    'generalError'           => 'حدث خطا ما',

];